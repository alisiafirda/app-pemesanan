package safira.alisia.apppemesanan

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_pesan.view.*


class FragmentPesan :  Fragment(), View.OnClickListener {
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnTambah -> {
                builder.setTitle("Konfirmasi").setMessage("Data yang akan dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya", btnInsertDialog)
                    .setNegativeButton("Tidak", null)
                builder.show()
            }
            R.id.btnHapus -> {
                builder.setTitle("Konfirmasi").setMessage("Yakin akan menghapus data ini?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya", btnDeleteDialog)
                    .setNegativeButton("Tidak", null)
                builder.show()
            }
            R.id.btnEdit -> {
                builder.setTitle("Konfirmasi").setMessage("Data yang akan dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya", btnUpdateDialog)
                    .setNegativeButton("Tidak", null)
                builder.show()
            }
        }
    }
    lateinit var thisParent: MainActivity
    lateinit var db : SQLiteDatabase
    lateinit var adapter : ListAdapter
    lateinit var v :View
    lateinit var builder : AlertDialog.Builder
    var idMakan : String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        db = thisParent.getDBObject()
        v = inflater.inflate(R.layout.frag_pesan,container,false)
        v.btnEdit.setOnClickListener(this)
        v.btnTambah.setOnClickListener(this)
        v.btnHapus.setOnClickListener(this)
        builder = AlertDialog.Builder(thisParent)
        v.lsPesan.setOnItemClickListener(itemClick)

        return v
    }
    fun showDataMakanan(){
        val cursor : Cursor = db.query("makan", arrayOf("nm_makan","jmlmakan","harga", "id_makan as _id"),
            null, null, null,null,"_id asc")
        adapter = SimpleCursorAdapter(thisParent,R.layout.item_pesan,cursor,
            arrayOf("_id","nm_makan","jmlmakan","harga"), intArrayOf(R.id.txNmMkn, R.id.txNmMkn,R.id.txJmlMakan, R.id.txHarga),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsPesan.adapter = adapter
    }
    override fun onStart() {
        super.onStart()
      //  showDataMakanan()
    }

    val itemClick = AdapterView.OnItemClickListener{ parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        idMakan = c.getString(c.getColumnIndex("_id"))
        v.namaMkn.setText(c.getString(c.getColumnIndex("nm_makan")))
        v.jmlMakan.setText(c.getString(c.getColumnIndex("jmlmakan")))
        v.harga.setText(c.getString(c.getColumnIndex("harga")))
    }
    fun insertDataMakan(nama : String, jumlah : String, harga : String){
        var cv : ContentValues = ContentValues()
        cv.put("nm_makan",nama)
        cv.put("jmlmakan",jumlah)
        cv.put("harga",harga)
        db.insert("makan", null,cv)
        //showDataMakanan()
    }
    fun deleteDataMakan(idMakan : String){
        db.delete("makan","id_makan = $idMakan", null)
        //showDataMakanan()
    }
    fun updateDataMakan(nama: String, jumlah: String, harga : String, idMakan: String){
        var cv : ContentValues = ContentValues()
        cv.put("nm_makan",nama)
        cv.put("jmlmakan",jumlah)
        cv.put("harga",harga)
        db.update("makan",cv,"id_makan = $idMakan",null)
        //showDataMakanan()
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        insertDataMakan(v.namaMkn.text.toString(), v.jmlMakan.text.toString(), v.harga.text.toString())
        v.namaMkn.setText("")
        v.jmlMakan.setText("")
        v.harga.setText("")
    }
    val btnDeleteDialog = DialogInterface.OnClickListener { dialog, which ->
        deleteDataMakan(idMakan)
        v.namaMkn.setText("")
        v.jmlMakan.setText("")
        v.harga.setText("")
    }
    val btnUpdateDialog = DialogInterface.OnClickListener { dialog, which ->
        updateDataMakan(v.namaMkn.text.toString(), v.jmlMakan.text.toString(), v.harga.text.toString(), idMakan)
        v.namaMkn.setText("")
        v.jmlMakan.setText("")
        v.harga.setText("")
    }


}