package safira.alisia.apppemesanan

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_menu.view.*


class FragmentMenu : Fragment(),View.OnClickListener {
    override fun onClick(v: View?) {

        }


    lateinit var thisParent : MainActivity
    lateinit var db : SQLiteDatabase
    lateinit var adapter : ListAdapter
    lateinit var v : View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        thisParent = activity as MainActivity
        db = thisParent.getDBObject()

        v =  inflater.inflate(R.layout.frag_menu,container,false)


        return v
    }

    fun showDataMenu(){
        val cursor : Cursor = db.query("menu", arrayOf("nama_menu", "kode_menu as _id"),
            null, null, null, null, "nama_menu asc")
        adapter = SimpleCursorAdapter(thisParent,R.layout.item_menu,cursor,
            arrayOf("_id", "nama_prodi"), intArrayOf(R.id.txIdMenu, R.id.txNamaMenu),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
    }

    override fun onStart() {
        super.onStart()
       // showDataMenu()
    }
}
