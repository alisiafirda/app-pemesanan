package safira.alisia.apppemesanan

import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var db : SQLiteDatabase
    lateinit var fragPesan : FragmentPesan
    lateinit var fragMenu : FragmentMenu
    lateinit var ft : FragmentTransaction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        fragPesan = FragmentPesan()
        fragMenu = FragmentMenu()
        db = DBOpenHelper(this).writableDatabase
    }

    fun getDBObject() : SQLiteDatabase {
        return db
    }
    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when(p0.itemId){
            R.id.itemMenu ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.FrameLayout,fragMenu).commit()
                FrameLayout.setBackgroundColor(Color.argb(245, 225, 255, 255))
                FrameLayout.visibility = View.VISIBLE
            }
            R.id.itemPesan ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.FrameLayout,fragPesan).commit()
                FrameLayout.setBackgroundColor(Color.argb(245, 225, 255, 255))
                FrameLayout.visibility = View.VISIBLE
            }
            R.id.itemBeranda -> FrameLayout.visibility = View.GONE
        }
        return true
    }
}
