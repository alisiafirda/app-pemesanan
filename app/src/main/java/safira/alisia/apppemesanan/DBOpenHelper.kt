package safira.alisia.apppemesanan

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context : Context): SQLiteOpenHelper(context,DB_Name, null, DB_Ver) {

    override fun onCreate(db: SQLiteDatabase?) {
        val tMenu = "create table menu(id_menu integer primary key , nm_menu text not null, detail_menu text not null, harga int not null)"
        val tMkn = "create table makan(id_makan integer primary key autoincrement,nm_makan text not null, jmlmakan text not null, harga varchar not null)"

        db?.execSQL(tMkn)
        db?.execSQL(tMenu)

    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

    companion object{
        val DB_Name = "menu"
        val DB_Ver = 1
    }
}